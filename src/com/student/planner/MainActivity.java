package com.student.planner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

public class MainActivity extends TabActivity {
	
	private boolean loadSuccessful = false;
	
	public  ArrayList<TabSpec> tabs;
	
	public  TabHost tabHost;
	
	public DataHandler dataHandler = null;
	public static ArrayList<String> tabOrder = null;
	public static HashMap<String, ArrayList<Task>> tasks = null;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        /** Load data */
        dataHandler = new DataHandler(this);
        if (dataHandler.loadData(this, "ioc.txt") == true) {
        	tasks 		= dataHandler.getTasks();
        	tabOrder 	= dataHandler.getTabOrder();
        	
        	ScheduleActivity.schedule = dataHandler.getSchedule();
        	loadSuccessful = true;
        	
        	Log.d ("LOAD", "" + tasks.size());
        	Log.d ("LOAD", "" + tasks.values());
        } else {
        	Log.d ("W", "Load error!");
        	tasks = new HashMap<String, ArrayList<Task>>();
        	tasks.put("Main Tasks", new ArrayList<Task>());
        	
        	tabOrder = new ArrayList<String>();
        	tabOrder.add("Main Tasks");
        	
        	ScheduleActivity.initSchedule();
        	
        	loadSuccessful = false;
        }
        
        tabHost = getTabHost();
       
        
        tabs = new ArrayList<TabHost.TabSpec>();
       
       
        
        for (int i = 0; i < tabOrder.size(); ++i) {
        	
        	TabSpec tabSpec = tabHost.newTabSpec(tabOrder.get(i));
        	tabSpec.setIndicator(tabOrder.get(i));
        	Intent tabIntent = new Intent (this, Tab.class);
        	tabIntent.putExtra("tab_name", tabOrder.get(i));
        	tabSpec.setContent(tabIntent);
        	
        	tabs.add(tabSpec);
        }
        
        TabSpec addNewGroupSpec = tabHost.newTabSpec ("Add");
        addNewGroupSpec.setIndicator("Add new group", getResources().getDrawable(R.drawable.add_task_tab_ico));
        Intent addTasksIntent = new Intent (this, TabAdder.class);
        addNewGroupSpec.setContent(addTasksIntent);
        
        //tabs.add(tasksSpec);
        tabs.add(addNewGroupSpec);
//        
//        for(int i = 0 ;i < tabs.size(); i ++){
//    		tabHost.addTab(tabs.get(i));
//    	}
        
        refreshTabs();
    }
    
    public void refreshTabs(){
    	tabHost = getTabHost();
    	tabHost.setCurrentTab(0); 
    	tabHost.clearAllTabs();
    	for(int i = 0 ;i < tabs.size() - 1; i ++){
     		tabHost.addTab(tabs.get(i));
     		tabHost.getTabWidget().getChildAt(i)
    		.setOnLongClickListener(MainActivity
    				.getGroupLongClickListner(this,tabs.get(i).getTag()));
//     		tabHost.setCurrentTab(i);
     	}
    	
    	//last tab "Add group" doesnt need long click listner
    	tabHost.addTab(tabs.get(tabs.size()-1));
    	Log.d("planner", "refreshed");
    	Log.d("planner", "tabs size:" + tabs.size());
    }
    
    public void addNewTab(TabSpec tab){
    	tabs.add(tabs.size()-1, tab);
    }
    
    public void onResume(){
    	super.onResume();
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	
    	if (tasks != null && tabOrder != null) {
    		dataHandler.writeData(this, "ioc.txt", tasks, tabOrder, ScheduleActivity.schedule);
    	} else {
    		System.out.println("Write error!");
    	}
    }
    
    //@Override
    public void onStop () {
    	super.onStop();
    	
    	if (tasks != null) {
    		dataHandler.writeData(this, "ioc.txt", tasks, tabOrder, ScheduleActivity.schedule);
    	} else {
    		Log.d ("TAG", "Write error!");
    	}
    }
    
    @Override
    public void onPause () {
    	super.onPause ();
    	
    	if (tasks != null) {
    		dataHandler.writeData(this, "ioc.txt", tasks, tabOrder, ScheduleActivity.schedule);
    	} else {
    		Log.d ("TAG", "Write error!");
    	}
    }
    
    public static OnLongClickListener getGroupLongClickListner(final Context c,
    		final String groupName){
	    return	new OnLongClickListener() {
				public boolean onLongClick(View v) {
					AlertDialog.Builder builder = new AlertDialog.Builder(c);
	            	
	            	builder.setTitle("Groups Menu");
	                builder.setItems(R.array.longClickGroup, new DialogInterface.OnClickListener() {
	                           public void onClick(DialogInterface dialog, int which) {
	                        	   
	                        	   switch(which){
	                        	   case 0://delete
	                        		   tasks.remove(groupName);
	                        		   tabOrder.remove(groupName);
	                        		   ArrayList<TabSpec> tabs = ((MainActivity)c).tabs;
	                        		   for (int i = 0;i < tabs.size(); i++){
	                        			   if (tabs.get(i).getTag().compareTo(groupName) == 0){
	                        				   tabs.remove(i);
	                        				   break;
	                        			   }
	                        		   }
	                        			   
	                        		   break;
	                        	   }
	                        	   ((MainActivity)c).refreshTabs();
	                       }
	                });
	                builder.create().show();
					return true;
				}
			};
    }
}
