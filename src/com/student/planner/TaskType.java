package com.student.planner;

import java.util.ArrayList;

public class TaskType {
	
	private String 				type;
	private ArrayList<Task> 	children;
	
	/**
	 * Constructors
	 */
	public TaskType (String type, ArrayList<Task> children) {
		
		this.type 		= type;
		this.children 	= children;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<Task> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<Task> children) {
		this.children = children;
	}
}
