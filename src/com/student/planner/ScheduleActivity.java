package com.student.planner;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;


public class ScheduleActivity extends TabActivity {
	
	public  ArrayList<TabSpec> tabs;
 	public  TabHost tabHost;
 	
 	public static final String daysOfWeek[] = {"L", "Ma", "Mi", "J", "V"};
 	
 	public static HashMap<String, ArrayList<Discipline>> schedule = null;
 	
 	public static void initSchedule () {
 		
 		schedule = new HashMap<String, ArrayList<Discipline>>();
 		for (int i = 0; i < 5; ++i) {
 			schedule.put(daysOfWeek[i], new ArrayList<Discipline>());
 		}
 	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule);
        
        Bundle extras = getIntent().getExtras();
//	    boolean loadSuccessful = extras.getBoolean("load");
        
        /** Init schedule */
	    /*if (loadSuccessful == false) { 
	    	this.initSchedule();
	    }*/
        
        tabHost = getTabHost();
        tabs = new ArrayList<TabHost.TabSpec>();
       
        for (int i = 0; i < 5; ++i) {
			TabSpec daySpec = tabHost.newTabSpec(daysOfWeek[i]);
			daySpec.setIndicator(daysOfWeek[i]);
			Intent dayIntent = new Intent (this, DayScheduleActivity.class);
			dayIntent.putExtra("tab_name", daysOfWeek[i]);
			daySpec.setContent(dayIntent);
        
			tabs.add(daySpec);
        }
        
        for(int i = 0 ;i < tabs.size(); i ++){
    		tabHost.addTab(tabs.get(i));
    	}
    }
}
