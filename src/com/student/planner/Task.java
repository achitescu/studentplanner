package com.student.planner;

import java.io.Serializable;
import java.util.Date;

public class Task implements Serializable{
	
	private static final long serialVersionUID = 7526471155622776147L;
	
	public Date dueDate, creationDate;
	public String description, name;
	public int priority, alarm_id;
	
	boolean checked = false ;  
	public boolean completed = false;
	
	public Task(String name){
		this.name = name;
	}
	public Task(String name, String descr, Date due, Date create, int priority){
		this.name = name;
		this.description = descr;
		this.dueDate = due;
		this.creationDate = create;
		this.priority = priority;
	}
	
	//Getters and setters
	public int getPriority(){
		return priority;
	}
	
	public String getDescription(){
		return description;
	}
	
	public String getName(){
		return name;
	}
	
	public Date getDueDate(){
		return dueDate;
	}
	
	public Date getCreationDate(){
		return creationDate;
	}
	
	public boolean isChecked() {
		
		return checked;
	}
	
	public void setChecked (boolean checked) {
    	
		this.checked = checked;
	}
	
	public void toggleChecked() {
		
      checked = !checked;
	}
}
