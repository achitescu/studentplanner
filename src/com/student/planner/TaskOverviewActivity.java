package com.student.planner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import android.os.Bundle;
import android.app.Activity;
import android.app.ExpandableListActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.support.v4.app.NavUtils;

public class TaskOverviewActivity extends ExpandableListActivity {
	
	private ExpandableListView 			mExpandableList;
	private MyExpandableListAdapter 	adapter;
	
	private MySet<Task> 				tasks;	
	
	public HashMap<Date, String> newTasks = new HashMap<Date, String>();
	
	public ArrayList <Task> urgent 		= new ArrayList <Task>();
	public ArrayList <Task> tomorrow 	= new ArrayList <Task>();
	public ArrayList <Task> thisWeek	= new ArrayList <Task>();
	public ArrayList <Task> overdue		= new ArrayList <Task>();
	
	public void getTasks () {
		
		tasks = new MySet<Task>();
		Set<Entry<String, ArrayList<Task>>> tabsSet = MainActivity.tasks.entrySet();
		Iterator<Entry<String, ArrayList<Task>>> it = tabsSet.iterator();
		
		while (it.hasNext() == true) {
			Map.Entry<String, ArrayList<Task>> tabTasks = 
									(Map.Entry<String, ArrayList<Task>>)it.next();
			
			/** Add tasks */
			for (int i = 0; i < tabTasks.getValue().size(); ++i) {
				Task task = tabTasks.getValue().get(i);
				tasks.add(task);
			}			
		}
	}
	
	public void getChildren () {
		
		/** Current date */
		Calendar now = Calendar.getInstance();
	    int day		= now.get(Calendar.DAY_OF_MONTH);
	    int month	= now.get(Calendar.MONTH);
	    int year	= now.get(Calendar.YEAR);
	    
	    int newDay, m, y;
	
	    this.getTasks();
	    
	    int nTasks = tasks.size();
	    for (int i = 0; i < nTasks; ++i) {
	    	
	    	Task task = tasks.get(i);
	    	
	    	/** Due date for task */
	    	Calendar cal = Calendar.getInstance();
			cal.setTime(task.getDueDate());
	    	newDay	= cal.get(Calendar.DAY_OF_MONTH);
			m		= cal.get(Calendar.MONTH);
			y		= cal.get(Calendar.YEAR);
			
	    	if(y == year) {
		    	if( m == month) {
		    		if( newDay == day) {
		    			urgent.add(task);
		    			thisWeek.add(task);
		    			Log.d ("URGENT", "task");
		    		}
		    		else if ( newDay > day){
		    			if (newDay-day ==1){
		    				tomorrow.add(task);
		    				thisWeek.add(task);
		    			} 
		    			else if ((newDay-day >=2) && (newDay-day <= 6) ){
		    				thisWeek.add(task);
		    			}
		    		} else if ( newDay < day) {
		    			overdue.add(task);
		    		}
		    	} else if (m - month == -1 ) {
		    		overdue.add(task);
		    	}
		    }
	    }
	}
	

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_overview);
        
        mExpandableList = getExpandableListView();
        
        ArrayList<TaskType> taskTypes = new ArrayList<TaskType>();
        
        this.getChildren();
        
        TaskType urgent 	= new TaskType("Urgent", this.urgent);
        TaskType tomorrow	= new TaskType("Tomorrow",this.tomorrow);
        TaskType thisWeek	= new TaskType("This Week", this.thisWeek);
        TaskType overdue	= new TaskType("Overdue", this.overdue);
        
        taskTypes.add (urgent);
        taskTypes.add (tomorrow);
        taskTypes.add (thisWeek);
        taskTypes.add (overdue); 
        
        /** Sets the adapter that provides data to the list. */
        adapter = new MyExpandableListAdapter (this, taskTypes);
        mExpandableList.setAdapter(adapter);
    }
}
