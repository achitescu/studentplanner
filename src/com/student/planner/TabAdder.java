package com.student.planner;

import java.util.ArrayList;

import android.R.layout;
import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost.TabSpec;

public class TabAdder extends Activity {
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);    
		setContentView(R.layout.add_tab);
		
		EditText editText = (EditText) findViewById(R.id.edit_tabName);
		editText.setText("");
		
		//DisplayExistingTasksActivity.selectedTasks = new ArrayList<Task>();
		
		Button selectButton = (Button) findViewById(R.id.button_select_existing_tasks);
		selectButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				
				if (DisplayExistingTasksActivity.selectedTasks != null) {
					DisplayExistingTasksActivity.selectedTasks.clear();
				}
				Intent intent = new Intent(TabAdder.this, DisplayExistingTasksActivity.class);
				startActivity(intent);
			}
		});
		
		/**
		 *  Add new tab.
		 */
		Button addTabButton = (Button) findViewById(R.id.button_add_tab);
		addTabButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				EditText editText = (EditText) findViewById(R.id.edit_tabName);
				String strTabName = editText.getText().toString();
				//TasksTab.tasks.put(strTabName, new ArrayList<Task>());
				MainActivity.tasks.put(strTabName, new ArrayList<Task>());
				TabSpec tabSpec = ((TabActivity)TabAdder.this.getParent()).getTabHost().newTabSpec(strTabName);
				tabSpec.setIndicator(strTabName);
				Intent tabIntent = new Intent (TabAdder.this, Tab.class);
		        //tabIntent.putExtra ("tab_name", "main");
				tabIntent.putExtra ("tab_name", strTabName);
		        tabSpec.setContent (tabIntent);
		        
		        
		        MainActivity.tabOrder.add(strTabName);
		        MainActivity.tasks.put(strTabName, new ArrayList<Task>());
//				MainActivity.tabs.add (tabSpec);
				((MainActivity)TabAdder.this.getParent()).addNewTab(tabSpec);
		        ((MainActivity)TabAdder.this.getParent()).refreshTabs();
			}
		});
    }
	
	/**
	  * Called when the user clicks the Select existing tasks button
	  *
	  */
	public void dispalyExistingTasks (View view) {
	    
		Intent intent = new Intent(this, DisplayExistingTasksActivity.class);
		startActivity(intent);
	}
	
	@Override
	public void onResume () {
		super.onResume ();
		EditText editText = (EditText) findViewById(R.id.edit_tabName);
		editText.setText("");
	}	
}
