package com.student.planner;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class MyExpandableListAdapter extends BaseExpandableListAdapter{
	
	
	private LayoutInflater inflater;
	private ArrayList<TaskType> taskTypes;
	//private String[] taskTypes = { "Urgent", "Today", "Tomorrow", "This Week", "Overdue" };
	private Context context;
	
	/**
	 * Constructors
	 */
	public MyExpandableListAdapter (
					Context context,
					ArrayList<TaskType> taskTypes) {
		
		this.context 		= context;
        this.taskTypes 		= taskTypes;
        this.inflater 		= LayoutInflater.from(context);
    }
	
	public Object getChild(int nGroupIndex, int nChildIndex) {
		return taskTypes.get(nGroupIndex).getChildren().get(nChildIndex);
	}

	public long getChildId(int nGroupIndex, int nChildIndex) {
		return nChildIndex;
	}

	public View getChildView (int i, int i1, boolean b, View view, ViewGroup viewGroup) {
		
        if (view == null) {
            view = inflater.inflate(R.layout.list_task_overview_child, viewGroup, false);
        }
        
        TaskType taskType = (TaskType) getGroup(i);
        
        TextView task = (TextView) view.findViewById(R.id.txtTaskOverviewChild);
        task.setText(taskType.getChildren().get(i1).getName());
		       
		return view;
    }

	public int getChildrenCount(int i) {
		return taskTypes.get(i).getChildren().size();
	}

	public Object getGroup(int i) {
		return taskTypes.get(i);
	}

	public int getGroupCount() {
		return this.taskTypes.size();
	}

	public long getGroupId(int nGroupIndex) {
		return nGroupIndex;
	}

	public View getGroupView (int i, boolean b, View view, ViewGroup viewGroup) {
		
		if (view == null) {
            view = inflater.inflate(R.layout.list_task_overview_parent, viewGroup, false);
        }
		
		/** Get task type */
		TaskType taskType = (TaskType) getGroup(i);
 
        TextView txtTaskType = (TextView) view.findViewById(R.id.txtTaskType);
        txtTaskType.setText (taskType.getType());
            
        return view;
	}

	public boolean hasStableIds() {
		return true;
	}

	public boolean isChildSelectable(int arg0, int arg1) {
		return true;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
   /* private String[][] list_tasks = {
            { "Task 3", "Task 4", "Task 5" },
            { "Task 4", "Task 5" },
            { "Task 6", "Task 7" },
            { "Task 1", "Task 2", "Task 3", "Task 4", "Task 5", "Task 6", "Task 7" },
            { "Task 3"}
    };
    
    public Object getChild(int groupPosition, int childPosition) {
		return list_tasks[groupPosition][childPosition];
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}
	
	public TextView getGenericView() {
        // Layout parameters for the ExpandableListView
        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, 64);

        TextView textView = new TextView(new MyExpandableList());
        textView.setLayoutParams(lp);
        // Center the text vertically
        textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
        // Set the text starting position
        textView.setPadding(36, 0, 0, 0);
        return textView;
    }

	public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
            View convertView, ViewGroup parent) {
		
		 TextView textView = getGenericView();
         textView.setText(getChild(groupPosition, childPosition).toString());
         return textView;
	}

	public int getChildrenCount(int groupPosition) {
		 return list_tasks[groupPosition].length;
	}

	public Object getGroup(int groupPosition) {
		return taskTypes[groupPosition];
	}

	public int getGroupCount() {
		 return taskTypes.length;
	}

	public long getGroupId(int groupPosition) {
		
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
            ViewGroup parent) {
		TextView textView = getGenericView();
        textView.setText(getGroup(groupPosition).toString());
        return textView;
	}

	public boolean hasStableIds() {
		
		return true;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		
		return true;
	}*/
	
	

}
