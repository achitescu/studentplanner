package com.student.planner;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class DayScheduleAdapter extends BaseExpandableListAdapter {
	
	private LayoutInflater inflater;
	private ArrayList<Discipline> disciplines;
	private Context context;
	private String dayOfWeek;
	private ExpandableListView expandableList;
	
	/**
	 * Constructors
	 */
	public DayScheduleAdapter (
					Context context,
					ArrayList<Discipline> disciplines,
					String dayOfWeek,
					ExpandableListView expandableList) {
		
		this.context 		= context;
        this.disciplines 	= disciplines;
        this.inflater 		= LayoutInflater.from(context);
        this.dayOfWeek		= dayOfWeek;
        this.expandableList = expandableList;
    }

	public Object getChild(int nGroupIndex, int nChildIndex) {
		return disciplines.get(nGroupIndex).getChildren().get(nChildIndex);
	}

	public long getChildId(int nGroupIndex, int nChildIndex) {
		return nChildIndex;
	}

	public View getChildView (int i, int i1, boolean b, View view, ViewGroup viewGroup) {
		
        if (view == null) {
            view = inflater.inflate(R.layout.list_discipline_child, viewGroup, false);
        }
        
        /*
        if (mParent.get(i).isHasAddOn() == true) {
 
		    TextView textView = (TextView) view.findViewById(R.id.list_item_text_child);
		    //"i" is the position of the parent/group in the list and
		    //"i1" is the position of the child
		    textView.setText(mParent.get(i).getAddOnChild());
        } else {
        	 TextView textView = (TextView) view.findViewById(R.id.list_item_text_child);
 		    //"i" is the position of the parent/group in the list and
 		    //"i1" is the position of the child
 		    textView.setText("");
        }*/
        
        Discipline discipline = (Discipline) getGroup(i);
        
        TextView disciplineLocation = (TextView) view.findViewById(R.id.txtChild);
        disciplineLocation.setText(discipline.getChildren().get(i1));
		       
		return view;
    }

	public int getChildrenCount(int i) {
		return disciplines.get(i).getChildren().size();
	}

	public Object getGroup(int i) {
		return disciplines.get(i);
	}

	public int getGroupCount() {
		return this.disciplines.size();
	}

	public long getGroupId(int nGroupIndex) {
		return nGroupIndex;
	}

	public View getGroupView (int i, boolean b, View view, ViewGroup viewGroup) {
		
		if (view == null) {
            view = inflater.inflate(R.layout.list_discipline_parent, viewGroup, false);
        }
		
		final int index = i;
		
		/** Get discipline */
		Discipline discipline = (Discipline) getGroup(i);
 
        TextView disciplineType = (TextView) view.findViewById(R.id.txtDisciplineType);
        disciplineType.setText(discipline.getType());
        
        TextView disciplineHour = (TextView) view.findViewById(R.id.txtDisciplineHour);
        disciplineHour.setText(discipline.getHour());
        
        TextView disciplineName = (TextView) view.findViewById(R.id.txtDisciplineName);
        disciplineName.setText(discipline.getName());
        
        view.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (expandableList.expandGroup(index) == false) {
					expandableList.collapseGroup(index);
				}
			}
        });
        
        view.setOnLongClickListener(new OnLongClickListener() {
			public boolean onLongClick(View v) {			
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("Discipline menu");
				builder.setItems (
						R.array.longClickDiscipline,
						new DialogInterface.OnClickListener() {
							public void onClick (DialogInterface dialog, int which) {
								switch(which){
								case 0:
									 Intent editDisciplineIntent = new Intent(context, AddDisciplineActivity.class);
									 editDisciplineIntent.putExtra("tab_name", dayOfWeek);
									 editDisciplineIntent.putExtra("index", index);
									 context.startActivity(editDisciplineIntent);
									 break;
	                        		   
								case 1:
									ScheduleActivity.schedule.get(dayOfWeek).remove(index); 
									break;
								}
								
								DayScheduleAdapter.this.notifyDataSetChanged();
								expandableList.invalidate();
							}
						});
				builder.create().show();
				return false;
			}
		});
        
        return view;
	}

	public boolean hasStableIds() {
		return true;
	}

	public boolean isChildSelectable(int arg0, int arg1) {
		return true;
	}
}
