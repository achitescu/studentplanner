package com.student.planner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.support.v4.app.NavUtils;
import android.text.format.Time;

public class AddDisciplineActivity extends Activity {
	
	static final int TIME_DIALOG_ID = 0;
	
	private Discipline discipline = null;
	
	private String 	dayOfWeek 		= null;
	private int 	disciplineIndex = -1;
	
	private TextView disciplineHour 		= null;
	private EditText disciplineName 		= null;
	private EditText disciplineLocation 	= null;
	private Button buttonPickHour			= null;
	
	private RadioButton radioButtonC		= null;
	private RadioButton radioButtonS		= null;
	private RadioButton radioButtonL		= null;
	
	private Button addDisciplineButton 		= null;
			
	
	private String selectedHour = null;
	
	private TimePickerDialog.OnTimeSetListener mTimeListener =
		    new TimePickerDialog.OnTimeSetListener() {
				public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
					
					SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
					
					Date dt = new Date(1989, 9, 25, hourOfDay, minute);
					selectedHour = timeFormat.format(dt);
					disciplineHour.setText(selectedHour);
				}
		    };
		    
    public String getSelectedHour () {
    	return this.selectedHour;
    }
		    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_discipline);
        
        /** Get the day of the week */
        Bundle extras 	= getIntent().getExtras();
	    dayOfWeek 		= extras.getString ("tab_name");
	    disciplineIndex = extras.getInt ("index", -1);
        
        discipline = new Discipline();
        
        disciplineName 		= (EditText) findViewById(R.id.txtDisciplineName);
        disciplineHour 		= (TextView) findViewById(R.id.txtDisciplineHour);
		disciplineLocation 	= (EditText) findViewById(R.id.txtDisciplineLocation);
        buttonPickHour 		= (Button) findViewById(R.id.buttonPickHour);
        
        radioButtonC		= (RadioButton) findViewById(R.id.radio_C);
        radioButtonS		= (RadioButton) findViewById(R.id.radio_S);
        radioButtonL		= (RadioButton) findViewById(R.id.radio_L);
        
        addDisciplineButton = (Button) findViewById(R.id.button_addDiscipline);
        
        radioButtonC.setChecked(true);
        discipline.setType("C");
        
        if (disciplineIndex != -1) {
        	
        	Discipline discipline = ScheduleActivity.schedule.get(dayOfWeek).get(disciplineIndex);
        	
        	disciplineName.setText(discipline.getName());
        	disciplineHour.setText(discipline.getHour());
        	disciplineLocation.setText(discipline.getLocation());
        	
        	String type = discipline.getType();
        	
        	if (type.compareTo("C") == 0) {
        		radioButtonC.setChecked(true);		
        	} else if (type.compareTo("S") == 0) {
        		radioButtonS.setChecked(true);
        	} else if (type.compareTo("L") == 0) {
        		radioButtonL.setChecked(true);
        	}
        	
        	addDisciplineButton.setText("Update");
        }
        
        buttonPickHour.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog (TIME_DIALOG_ID);
			}
		});
        	
        /**
		 *  Add new discipline button
		 */
		addDisciplineButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				
				/**
				 * Add new discipline
				 */
				discipline.setName(disciplineName.getText().toString());
				discipline.setHour(disciplineHour.getText().toString());
				discipline.setLocation(disciplineLocation.getText().toString());
				
				ArrayList<Discipline> daySchedule = ScheduleActivity.schedule.get(dayOfWeek);
				
				if (disciplineIndex != -1) {
					daySchedule.set(disciplineIndex, discipline);
				} else {
					daySchedule.add(discipline);
				}
				
				finish();
			}
		});
		
		
		/** Click on cancel button */
		Button cancelButton = (Button) findViewById(R.id.button_cancelDiscipline);
		cancelButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
    }
    
	@Override
	protected Dialog onCreateDialog (int id) {
		
		switch (id) {
	    case TIME_DIALOG_ID:
	    	Calendar c = Calendar.getInstance();
	        int mHour = c.get(Calendar.HOUR_OF_DAY);
	        int mMinute = c.get(Calendar.MINUTE);
	        		
	    	return new TimePickerDialog (this, mTimeListener, mHour, mMinute, true);
		}
	    	
	    return null;
	}
    
    public void onRadioButtonClicked (View view) {
    	
    	if (radioButtonC.isChecked()) {
    		discipline.setType("C");
    	} else if (radioButtonS.isChecked()) {
    		discipline.setType("S");
    	} else if (radioButtonL.isChecked()) {
    		discipline.setType("L");
    	}
    	
        /** Is the button now checked? *
        boolean checked = ((RadioButton)view).isChecked();
        
		/** Check which radio button was clicked *
        switch(view.getId()) {
            case R.id.radio_C:
               if (checked) {
            	   discipline.setType("C");
               }
               
               break;
                
            case R.id.radio_S:
                if (checked) {
                	discipline.setType("S");
                }
                    
                break;
                
            case R.id.radio_L:
            	if (checked) {
            		discipline.setType("L");
            	}
        
        		break;
        		
        	default:
        		break;
        }*/
    }
}
