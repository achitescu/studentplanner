package com.student.planner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;

public class DataHandler {
	
	private HashMap<String, ArrayList<Task>> 	tasks 			= null;
	private ArrayList<String> 					tabOrder 		= null;
	
	private HashMap<String, ArrayList<Discipline>>	schedule 	= null;
	
	private Context context										= null;
	
	/**
	 * Constructors
	 */
	public DataHandler (Context context) {
		
		this.context = context;
	}
	
	public HashMap<String, ArrayList<Task>> getTasks() {
		return tasks;
	}

	public void setTasks(HashMap<String, ArrayList<Task>> tasks) {
		this.tasks = tasks;
	}

	public ArrayList<String> getTabOrder() {
		return tabOrder;
	}

	public void setTabOrder(ArrayList<String> tabOrder) {
		this.tabOrder = tabOrder;
	}

	public HashMap<String, ArrayList<Discipline>> getSchedule() {
		return schedule;
	}

	public void setSchedule(HashMap<String, ArrayList<Discipline>> schedule) {
		this.schedule = schedule;
	}

	public boolean loadData (Activity activity, String fileName) {
		
		String path = fileName;
		
		//String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
		//File f = new File(baseDir + File.separator + fileName);
		
		try {
			FileInputStream fileIn = context.openFileInput(fileName);
			//FileInputStream fileIn = new FileInputStream (f);
			ObjectInputStream in = new ObjectInputStream (fileIn);
           
			/** Load data */
			tasks 		= (HashMap<String, ArrayList<Task>>) in.readObject();
			tabOrder	= (ArrayList<String>) in.readObject();
			schedule	= (HashMap<String, ArrayList<Discipline>>) in.readObject();
			
			in.close();
			fileIn.close();
		}catch(IOException i) {
		   i.printStackTrace();
		   return false;
		} catch(ClassNotFoundException c) {
			c.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public void writeData (
						Activity activity,
						String fileName,
						HashMap<String, ArrayList<Task>> tasks, 
						ArrayList<String> tabOrder,
						HashMap<String, ArrayList<Discipline>> schedule) {
		
		String path = fileName;
		
		//String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
		//File f = new File(baseDir + File.separator + fileName);
		
		try {
			FileOutputStream fileOut = context.openFileOutput (path, Context.MODE_PRIVATE);
			//FileOutputStream fileOut = new FileOutputStream(f);
			ObjectOutputStream out = new ObjectOutputStream (fileOut);
			
			/** Write data */
			out.writeObject (tasks);
			out.writeObject (tabOrder);
			out.writeObject (schedule);
			
			/** Close file */
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
}
