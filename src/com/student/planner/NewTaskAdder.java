package com.student.planner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class NewTaskAdder extends Activity {
	
	static final int DATE_DIALOG_ID = 0;
	
	private TextView txtDate;
	private EditText edtName, edtDescription;
	private Button btnPickDate, btnAdd, btnCancel;
	private RatingBar rtbPriority;
	private Date selectedDate;
	private String tabName;
	private int taskIndex;
		
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_task);
		Bundle extras = getIntent().getExtras();
	    tabName = extras.getString("tab_name");
	    taskIndex = extras.getInt("index", -1);
	    
		
		txtDate = (TextView) findViewById(R.id.txtDueDate);
		edtName = (EditText) findViewById(R.id.add_taskName);
		edtDescription = (EditText) findViewById(R.id.add_taskDescription);
		btnPickDate = (Button) findViewById(R.id.btn_pick_date);
		btnAdd = (Button) findViewById(R.id.btn_add_task_save);
		btnCancel = (Button) findViewById(R.id.btn_add_task_cancel);
		rtbPriority = (RatingBar)findViewById(R.id.ratingBar1);
		btnPickDate.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				showDateTimeDialog();
			}
		});
		
		if (taskIndex != -1){
			selectedDate = MainActivity.tasks.get(tabName).get(taskIndex).dueDate;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
			txtDate.setText(sdf.format(selectedDate));
			edtName.setText(MainActivity.tasks.get(tabName).get(taskIndex).name);
			edtDescription.setText(MainActivity.tasks.get(tabName).get(taskIndex).description);
			rtbPriority.setRating(MainActivity.tasks.get(tabName).get(taskIndex).priority);
			btnAdd.setText("Update");
	    }
		
		
		btnAdd.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Task task = new Task(
						edtName.getText().toString(),
						edtDescription.getText().toString(),
						selectedDate,
						selectedDate,
						(int)rtbPriority.getRating()
						);
				
				//Tab.tasks.get(tabName).add(task);
				Log.d ("TASK", tabName);
				if (taskIndex != -1){
					MainActivity.tasks.get(tabName).set(taskIndex, task);
					task.alarm_id = MainActivity.tasks.get(tabName).get(taskIndex).alarm_id;
					Intent intent = new Intent(NewTaskAdder.this, AlarmReceiverActivity.class);
					intent.putExtra("name", task.getName());
					intent.putExtra("description", task.getDescription());
					
			        PendingIntent pendingIntent = PendingIntent.getActivity(NewTaskAdder.this,
			            task.alarm_id, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			        AlarmManager am = 
			            (AlarmManager)getSystemService(Activity.ALARM_SERVICE);
			        am.set(AlarmManager.RTC_WAKEUP, selectedDate.getTime(),
			                pendingIntent);
				}
				else{
					task.alarm_id = (int) selectedDate.getTime()/10000;
					MainActivity.tasks.get(tabName).add(task);
					Intent intent = new Intent(NewTaskAdder.this, AlarmReceiverActivity.class);
					intent.putExtra("name", task.getName());
					intent.putExtra("description", task.getDescription());
			        PendingIntent pendingIntent = PendingIntent.getActivity(NewTaskAdder.this,
			        		task.alarm_id, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			        AlarmManager am = 
			            (AlarmManager)getSystemService(Activity.ALARM_SERVICE);
			        am.set(AlarmManager.RTC_WAKEUP, selectedDate.getTime(),
			                pendingIntent);
				}
						
				NewTaskAdder.this.finish();
			}
			
		});
		
		btnCancel.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				NewTaskAdder.this.finish();
			}
		});
		
    }
	
		
	private void showDateTimeDialog() {
        // Create the dialog
        final Dialog mDateTimeDialog = new Dialog(this);
        // Inflate the root layout
        final RelativeLayout mDateTimeDialogView = (RelativeLayout) getLayoutInflater().inflate(R.layout.date_time_dialog, null);
        // Grab widget instance
        final DateTimePicker mDateTimePicker = (DateTimePicker) mDateTimeDialogView.findViewById(R.id.DateTimePicker);
        // Check is system is set to use 24h time (this doesn't seem to work as expected though)
        final String timeS = android.provider.Settings.System.getString(getContentResolver(), android.provider.Settings.System.TIME_12_24);
        final boolean is24h = !(timeS == null || timeS.equals("12"));
        
        // Update demo TextViews when the "OK" button is clicked 
        ((Button) mDateTimeDialogView.findViewById(R.id.SetDateTime)).setOnClickListener(new OnClickListener() {

                public void onClick(View v) {
                        mDateTimePicker.clearFocus();
                        String date = mDateTimePicker.get(Calendar.YEAR) + "/" + (mDateTimePicker.get(Calendar.MONTH)+1) + "/"
                                + mDateTimePicker.get(Calendar.DAY_OF_MONTH);
                        String time;
                        
                        if (mDateTimePicker.is24HourView()) {
                                time = mDateTimePicker.get(Calendar.HOUR_OF_DAY) + ":" + mDateTimePicker.get(Calendar.MINUTE);
                        } else {
                                time = mDateTimePicker.get(Calendar.HOUR) + ":" + mDateTimePicker.get(Calendar.MINUTE) + " "
                                                + (mDateTimePicker.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM");
                        }
                        txtDate.setText(date + " " + time);
                        selectedDate = new Date(date + " " + time);
                        Log.d("planner", selectedDate.toGMTString());
                        mDateTimeDialog.dismiss();
                }
        });

        // Cancel the dialog when the "Cancel" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.CancelDialog)).setOnClickListener(new OnClickListener() {

                public void onClick(View v) {
                        // TODO Auto-generated method stub
                        mDateTimeDialog.cancel();
                }
        });

        // Reset Date and Time pickers when the "Reset" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.ResetDateTime)).setOnClickListener(new OnClickListener() {

                public void onClick(View v) {
                        // TODO Auto-generated method stub
                        mDateTimePicker.reset();
                }
        });
        
        // Setup TimePicker
        mDateTimePicker.setIs24HourView(is24h);
        // No title on the dialog window
        mDateTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Set the dialog content view
        mDateTimeDialog.setContentView(mDateTimeDialogView);
        // Display the dialog
        mDateTimeDialog.show();
	}
	
}
