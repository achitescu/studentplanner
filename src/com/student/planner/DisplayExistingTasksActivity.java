package com.student.planner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.student.planner.ExistingTasksAdapter.TaskHolder;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

public class DisplayExistingTasksActivity extends Activity {
	
	private ListView listView;
	private ExistingTasksAdapter adapter;
	public static ArrayList<Task> selectedTasks = null;
	
	public static MySet<Task> tasks = null;
	 
	public MySet<Task> getExistingTasks()
	{
		MySet<Task> tasks = new MySet<Task>();
		
		//Set tabsSet = Tab.tasks.entrySet();
		Set<Entry<String, ArrayList<Task>>> tabsSet = MainActivity.tasks.entrySet();
		
		Iterator<Entry<String, ArrayList<Task>>> it = tabsSet.iterator();
		
		while (it.hasNext() == true) {
			Map.Entry<String, ArrayList<Task>> tabTasks = 
									(Map.Entry<String, ArrayList<Task>>)it.next();
			
			/** Add tasks */
			for (int i = 0; i < tabTasks.getValue().size(); ++i) {
				Task task = tabTasks.getValue().get(i);
				if (!task.completed) {
					tasks.add(task);
				}
			}			
		}
			
		return tasks;
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
   
        super.onCreate(savedInstanceState);
        setContentView(R.layout.existing_tasks);
        
        listView = (ListView) findViewById(R.id.existingTaskslist);
        
        tasks = this.getExistingTasks();
        
        adapter = new ExistingTasksAdapter (this, R.layout.existing_tasks_list_row, tasks);
        
        View header = (View)getLayoutInflater().inflate(R.layout.existing_tasks_list_header, null);
        listView.addHeaderView(header);
        
        /** When item is tapped, toggle checked properties of CheckBox and Task */  
        listView.setOnItemClickListener (new AdapterView.OnItemClickListener() {
		          public void onItemClick (AdapterView<?> parent, View item,
		                                   				int position, long id) {  
		            Task task = adapter.getItem( position );  
		            task.toggleChecked ();  
		            TaskHolder viewHolder = (TaskHolder) item.getTag();
		            viewHolder.checkTask.setChecked (task.isChecked());
		            
		            if (task.isChecked() == true) {
		            	selectedTasks.add(task);
		            }
		          }
        });
        
        /** Assign adapter to ListView */
        listView.setAdapter(adapter);
        
        /** Click on save button */
        Button saveButton = (Button) findViewById(R.id.button_save_tasks);
		saveButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				finish();
			}
		});
		
		/** Click on cancel button */
		Button cancelButton = (Button) findViewById(R.id.button_cancel);
		cancelButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				DisplayExistingTasksActivity.selectedTasks.clear();
				finish();
			}
		});
        
    }
    
    @Override
	public void onResume () {
		super.onResume();
		adapter.uncheckTasks();
		DisplayExistingTasksActivity.selectedTasks = new ArrayList<Task>();
	}
}
