package com.student.planner;

import java.util.ArrayList;
import java.util.Collections;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.app.ExpandableListActivity;
import android.content.Intent;

public class DayScheduleActivity extends ExpandableListActivity {

	private ExpandableListView mExpandableList;
	private DayScheduleAdapter adapter;
	
	private String dayOfWeek;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.day_schedule);
        
        mExpandableList = getExpandableListView();
        
        /** Get the day of the week */
        Bundle extras = getIntent().getExtras();
	    dayOfWeek = extras.getString("tab_name");
	    
	    /** Get disciplines for current day of the week */
	    ArrayList<Discipline> disciplines = ScheduleActivity.schedule.get(dayOfWeek);
	    
	    Collections.sort(disciplines);
       
        /** Set the parents and the children */
        int nSize = disciplines.size();
        for (int i = 0; i < nSize; ++i) {
        	Discipline discipline = disciplines.get(i);
        	ArrayList<String> children = new ArrayList<String>();
        	children.add(discipline.getLocation());
        	discipline.setChildren(children);
        }
        
        /** Sets the adapter that provides data to the list. */
        adapter = new DayScheduleAdapter (this, disciplines, dayOfWeek, mExpandableList);
        mExpandableList.setAdapter(adapter);
        
        
        /** Click on "Add Discipline" button */
        Button addDisciplineButton = (Button) findViewById (R.id.button_add_discipline);
        addDisciplineButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				Intent addDisciplineIntent = new Intent(DayScheduleActivity.this, AddDisciplineActivity.class);
				addDisciplineIntent.putExtra("tab_name", dayOfWeek);
				startActivity(addDisciplineIntent);
			}
		});
        
    }
    
    @Override
    public void onResume () {
    	
    	super.onResume();
    	
    	/** Get disciplines for current day of the week */
	    ArrayList<Discipline> disciplines = ScheduleActivity.schedule.get(dayOfWeek);
	    
	    Collections.sort(disciplines);
       
        /** Set the parents and the children */
        int nSize = disciplines.size();
        for (int i = 0; i < nSize; ++i) {
        	Discipline discipline = disciplines.get(i);
        	ArrayList<String> children = new ArrayList<String>();
        	children.add(discipline.getLocation());
        	discipline.setChildren(children);
        }
        
        /** Sets the adapter that provides data to the list. */
        adapter = new DayScheduleAdapter (this, disciplines, dayOfWeek, mExpandableList);
        mExpandableList.setAdapter(adapter);
    }
}
