package com.student.planner;

import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;

import 	java.text.DateFormat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;

public class TaskExpandableListAdapter extends BaseExpandableListAdapter {

	private Context mContext;
	private ExpandableListView mExpandableListView;
	public List<Task> mGroupCollection;
	private int[] groupStatus;
	private String tabName;

	public TaskExpandableListAdapter(Context pContext,String tabName,
			ExpandableListView pExpandableListView,
			List<Task> pGroupCollection) {
		mContext = pContext;
		mGroupCollection = pGroupCollection;
		mExpandableListView = pExpandableListView;
		groupStatus = new int[mGroupCollection.size()];
		this.tabName = tabName;

		setListEvent();
	}
	

	private void setListEvent() {

		mExpandableListView
				.setOnGroupExpandListener(new OnGroupExpandListener() {

					public void onGroupExpand(int arg0) {
						// TODO Auto-generated method stub
						groupStatus[arg0] = 1;
					}
				});

		mExpandableListView
				.setOnGroupCollapseListener(new OnGroupCollapseListener() {

					public void onGroupCollapse(int arg0) {
						// TODO Auto-generated method stub
						groupStatus[arg0] = 0;
					}
				});
	}

	public String getChild(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return mGroupCollection.get(arg0).getDescription();
	}

	public long getChildId(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getChildView(int arg0, int arg1, boolean arg2, View arg3,
			ViewGroup arg4) {
		// TODO Auto-generated method stub

		ChildHolder childHolder;
		if (arg3 == null) {
			arg3 = LayoutInflater.from(mContext).inflate(
					R.layout.tasks_list_row, null);

			childHolder = new ChildHolder();

			childHolder.description = (TextView) arg3.findViewById(R.id.item_description);
			childHolder.dueDate = (TextView) arg3.findViewById(R.id.item_duedate);
			arg3.setTag(childHolder);
		}else {
			childHolder = (ChildHolder) arg3.getTag();
		}

		DateFormat df = DateFormat.getDateTimeInstance();
		childHolder.description.setText(mGroupCollection.get(arg0).getDescription());
		childHolder.dueDate.setText(df.format(mGroupCollection.get(arg0).getDueDate()));
		
		return arg3;
	}

	public int getChildrenCount(int arg0) {
		if (mGroupCollection.get(arg0).completed)
			return 0;
		return 1;
	}

	public Object getGroup(int arg0) {
		// TODO Auto-generated method stub
		return mGroupCollection.get(arg0);
	}

	public int getGroupCount() {
		// TODO Auto-generated method stub
		return mGroupCollection.size();
	}

	public long getGroupId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		final int index = groupPosition;
		TaskHolder groupHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.list_group,
					null);
			groupHolder = new TaskHolder();
			groupHolder.priorityIcon = (ImageView) convertView.findViewById(R.id.tag_img);
			groupHolder.txtName = (TextView) convertView.findViewById(R.id.group_title);
			convertView.setTag(groupHolder);
		} else {
			groupHolder = (TaskHolder) convertView.getTag();
		}
		
		convertView.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(groupStatus[index] == 0)
					mExpandableListView.expandGroup(index);
				else 
					mExpandableListView.collapseGroup(index);
			}
		});
		
		convertView.setOnLongClickListener(new OnLongClickListener() {
            public boolean onLongClick(View v) {
            	AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            	int id_options;
            	if (mGroupCollection.get(index).completed)
            		id_options = R.array.long_click_completed;
            	else
            		id_options = R.array.long_click;
            	
            	builder.setTitle("Task menu");
                builder.setItems(id_options, new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int which) {
                        	   Log.d("planner", which + "");
                        	   switch(which){
                        	   case 0: 
                        		   MainActivity.tasks.get(tabName).get(index).completed = 
                        				   !MainActivity.tasks.get(tabName).get(index).completed;
                        		   break;
                        	   case 1:
                        		   MainActivity.tasks.get(tabName).remove(index); 
                        		   break;
                        	   case 2:
                        		   Intent intentAddTask = new Intent(mContext, NewTaskAdder.class);
	                   		       intentAddTask.putExtra("tab_name", 
	                   		    		((MainActivity)((Tab) mContext).getParent()).tabHost.getCurrentTabTag());
	                   		       intentAddTask.putExtra("index", index);
	                   		       mContext.startActivity(intentAddTask);
                        		   break;
                        	   }
                        	   
                        	   TaskExpandableListAdapter.this.notifyDataSetChanged();
                        	   mExpandableListView.invalidate();
                       }
                });
                builder.create().show();
                return false;
            }
        });
		
		switch(mGroupCollection.get(groupPosition).getPriority()){
			case 0:
				groupHolder.priorityIcon.setImageResource(R.drawable.blue);
				break;
			case 1:
				groupHolder.priorityIcon.setImageResource(R.drawable.green);
				break;
			case 2:
				groupHolder.priorityIcon.setImageResource(R.drawable.yellow);
				break;
			case 3:
				groupHolder.priorityIcon.setImageResource(R.drawable.red);
				break;
		}
		
		groupHolder.txtName.setText(mGroupCollection.get(groupPosition).getName());
		int paintFlags = groupHolder.txtName.getPaintFlags();
		if (mGroupCollection.get(groupPosition).completed)
			groupHolder.txtName.setPaintFlags(paintFlags | Paint.STRIKE_THRU_TEXT_FLAG);
		else{
			if ((paintFlags & Paint.STRIKE_THRU_TEXT_FLAG) > 0)
				groupHolder.txtName.setPaintFlags(paintFlags ^ Paint.STRIKE_THRU_TEXT_FLAG);
		}

		return convertView;
	}

	class TaskHolder{
          ImageView priorityIcon;
          TextView txtName;
	}
	

	class ChildHolder {
		TextView description;
		TextView dueDate;
	}

	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return true;
	}

}
