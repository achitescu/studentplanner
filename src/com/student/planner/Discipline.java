package com.student.planner;

import java.io.Serializable;
import java.util.ArrayList;

import android.util.Log;

public class Discipline implements Serializable, Comparable<Discipline> {
	
	private static final long serialVersionUID = 7526471155622776149L;
	
	private String 					name;
	private String 					type;
	private String 					hour;
	private String 					location;
	
	private ArrayList<String> 		children;
	
	/**
	 * Constructors
	 */
	public Discipline () {
		
		this.name		= "";
		this.type	 	= "";
		this.hour 		= "";
		this.location 	= "";
	}
	
	public Discipline (
			String name,
			String type,
			String hour,
			String location) {
		
		this.name 		= name;
		this.type 		= type;
		this.hour 		= hour;
		this.location 	= location;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public ArrayList<String> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<String> children) {
		this.children = children;
	}

	public String transformHour (String hour) {
		
		String newHour = hour;
		
		if (newHour.contains("PM") == true) {
			int h = Integer.parseInt(newHour.substring(0, 2));
			h += 12;
			String hh = Integer.toString(h);
			newHour = hh.concat(newHour.substring(2));
		}
		
		return newHour;
	}

	public int compareTo(Discipline discipline) {
		
		String hour1 = null, hour2 = null;
		
		String newHour1 = transformHour(hour);
		String newHour2 = transformHour(discipline.getHour());
		
		Log.d ("COMP", newHour1 + " " + newHour2);
		
		hour1 = newHour1.substring(0, newHour1.length() - 3);
		hour2 = newHour2.substring(0, newHour2.length() - 3);
		
		return hour1.compareTo(hour2);
	}
}
