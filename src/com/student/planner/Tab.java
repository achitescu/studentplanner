package com.student.planner;

import java.util.ArrayList;
import java.util.HashMap;

import com.student.planner.TaskExpandableListAdapter.TaskHolder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Tab extends Activity {
		private String tabName;
		public ExpandableListView listView;
		TaskExpandableListAdapter adapter;
		
		public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.tasks);
	        
	        Bundle extras = getIntent().getExtras();
	        tabName = extras.getString("tab_name");
	        
	        if (DisplayExistingTasksActivity.selectedTasks != null) {
	        	Log.d ("TABNAME", "wrong");
	        	ArrayList<Task> selected_tasks = new ArrayList<Task>();
		    	for (int i = 0; i < DisplayExistingTasksActivity.selectedTasks.size (); ++i) {
		    		selected_tasks.add(DisplayExistingTasksActivity.selectedTasks.get(i));
		    	}
	        	MainActivity.tasks.put(tabName, selected_tasks);
	        	DisplayExistingTasksActivity.selectedTasks = null;
	        } else {
	        	Log.d ("TABNAME", tabName);
	        	if (MainActivity.tasks.get(tabName) == null) {
	        		MainActivity.tasks.put(tabName, new ArrayList<Task>());
	        	}
	        }
	        
	        listView = (ExpandableListView) findViewById(R.id.expandableListView);
	        
	    }
		
	    public void onResume(){
	    	super.onResume();
	    	
	    	adapter = new TaskExpandableListAdapter(this,tabName,
					listView, MainActivity.tasks.get(tabName));
	    	
	    	Log.d ("ONRESUME", "" + MainActivity.tasks.values ());

			// Assign adapter to ListView
	    	listView.setAdapter(adapter); 
	    }
	    
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        getMenuInflater().inflate(R.menu.activity_main, menu);
	        return true;
	    }
	    
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	    	int groupCount  = adapter.getGroupCount();
	    	switch(item.getItemId()){
			case R.id.add_task_menu:
			    	Intent intentAddTask = new Intent(this, NewTaskAdder.class);
			        intentAddTask.putExtra("tab_name", tabName);
			        startActivity(intentAddTask);
			        break;
			        
			case R.id.task_overview:
					Intent intentTaskOverview = new Intent (this, TaskOverviewActivity.class);
			        startActivity(intentTaskOverview);
			        break;
			
			case R.id.schedule_menu:
					Intent intentAddSchedule = new Intent (this, ScheduleActivity.class);
					startActivity (intentAddSchedule);
					break;       
		    case R.id.expand:
		    		for (int i = 0; i < groupCount; i++)
		    			listView.expandGroup(i);
					break;       
			case R.id.collapse:
					for (int i = 0; i < groupCount; i++)
		    			listView.collapseGroup(i);
					break;       
	    	}
	    	return super.onOptionsItemSelected(item);
	    }
	    
}
