package com.student.planner;

import java.util.ArrayList;

public class MySet<Task> extends ArrayList<Task>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public boolean add (Object o) {
		
		Task task = (Task) o;
		
		if (super.contains(task) == false) {
			super.add(task);
		}
		
		return false;
	}
}
