package com.student.planner;

import java.util.ArrayList;
import java.util.Set;

import com.student.planner.TaskExpandableListAdapter.TaskHolder;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class ExistingTasksAdapter extends ArrayAdapter<Task> {
	
	Context context; 
    int layoutResourceId;    
    MySet<Task> data = null;

	public ExistingTasksAdapter(Context context, int layoutResourceId, MySet<Task> data) {
		
		super(context, layoutResourceId, data);
		
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.data = data;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		/** Task to display */
		Task task = data.get(position);
		
		 View row = convertView;
		 TaskHolder holder = null;
		 
		 if (row == null) {
			 LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			 row = inflater.inflate(layoutResourceId, parent, false);
			 
			 holder = new TaskHolder();
			 holder.txtTaskName = (TextView)row.findViewById(R.id.txtTaskName);
			 holder.checkTask = (CheckBox)row.findViewById(R.id.checkbox_task);
			 holder.checkTask.setChecked(task.isChecked());
			 row.setTag (holder);
			 
			 /** If CheckBox is toggled, update the task it is tagged with. */  
			 holder.checkTask.setOnClickListener (new View.OnClickListener() {  
				  public void onClick(View v) {  
				    CheckBox cb = (CheckBox) v;
				    Task task = (Task) cb.getTag();
				    task.setChecked (cb.isChecked());
				    if (task.isChecked() == true) {
				    	DisplayExistingTasksActivity.selectedTasks.add(task);
				    } else {
				    	if (DisplayExistingTasksActivity.selectedTasks.contains(task)) {
				    		DisplayExistingTasksActivity.selectedTasks.remove(task);
				    	}
				    }
				  }  
			 });      
		 }
		 else {
			 holder = (TaskHolder)row.getTag();
		 }
	 
		/** 
		 * Tag the CheckBox with the Task it is displaying, so that we can  
		 * access the task in onClick() when the CheckBox is toggled.
		 */
		 holder.checkTask.setTag (task);
		 
		 /** Display task data */
		 holder.txtTaskName.setText (task.getName());
		 holder.checkTask.setChecked (task.isChecked());  
	 
		 //holder.priorityIcon.setImageResource(R.drawable.ic_launcher);
		 //holder.priorityIcon.setImageDrawable(this.context.getResources().getDrawable(R.drawable.ic_tab_example_selected));
		 
		 
		 return row;
	}
	
	public void uncheckTasks () {
		
		for (int i = 0; i < data.size(); ++i)
		{
			data.get(i).setChecked(false);
		}
	}

	class TaskHolder {
		CheckBox checkTask;
		TextView txtTaskName;
	}

}
